	<?php
	/* The loop starts here. */
	$postGridCount = 0;

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			$postGridCount++;

			// ************** Matteo
			//$author_avatar_url = get_user_meta(get_the_author_meta( 'ID' ), "listingpro_author_img_url", true);
			
			$get_author_id = get_the_author_meta('ID');
			$author_avatar_url = get_wp_user_avatar_src( get_the_author_meta( 'ID' ), $get_author_id );
			// ***********end Matteo


			$avatar ='';
			if(!empty($author_avatar_url)) {
				$avatar =  $author_avatar_url;

			} else {
				// ************** Matteo
				//$avatar_url = listingpro_get_avatar_url (get_the_author_meta( 'ID' ), $size = '51' );
				$get_author_id = get_the_author_meta('ID');
				$avatar_url = get_avatar( get_the_author_meta( 'ID' ), $get_author_id );
				// ***********end Matteo
				$avatar =  $avatar_url;

			}
			?>
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="col-md-4 col-sm-4 lp-blog-grid-box">
										<div class="lp-blog-grid-box-container lp-border lp-border-radius-8">
											<div class="lp-blog-grid-box-thumb">
												<a href="<?php the_permalink(); ?>">
													<?php

														if ( has_post_thumbnail() ) {
															the_post_thumbnail('listingpro-blog-grid');
														}
														else {
															echo '<img src="'.esc_html__('https://placeholdit.imgix.net/~text?txtsize=33&w=372&h=240', 'listingpro').'" alt="">';
														}
													?>
												</a>
											</div>
											<div class="lp-blog-grid-box-description text-center">
													<div class="lp-blog-user-thumb margin-top-subtract-25">
														<img class="avatar" src="<?php echo esc_url($avatar); ?>" alt="">
													</div>

													<div class="lp-blog-grid-category">
														<a href="blog-archive.html" >
															<?php the_category(' ,'); ?>
														</a>
													</div>
													<div class="lp-blog-grid-title">
														<h4 class="lp-h4">
															<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h4>
													</div>
													<ul class="lp-blog-grid-author">
														<li>

															<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
																<i class="fa fa-user"></i>
																<span><?php the_author(); ?></span>
															</a>
														</li>
														<li>
															<i class="fa fa-calendar"></i>
															<span><?php the_date(get_option('date_format')); ?></span>
														</li>
													</ul><!-- ../lp-blog-grid-author -->
											</div>
										</div>
									</div>
								</div>
		<?php
			if($postGridCount%3 == 0){
				echo '<div class="clearfix"></div>';
			}
		} // end while
		echo listingpro_pagination();
	}else{
		?>
		<div class="text-center margin-top-80 margin-bottom-100">
			<h2><?php esc_html_e('No Results','listingpro'); ?></h2>
			<p><?php esc_html_e('Sorry! There are no posts matching your search.','listingpro'); ?></p>
			<p><?php esc_html_e('Try changing your search Keyword','listingpro'); ?>
			</p>
		</div>
		<?php
	} // end if
