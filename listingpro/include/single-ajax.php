<?php
/**
 * Claim List
 *
 */

/* ============== ListingPro single ajax Init ============ */
	
	if(!function_exists('Listingpro_single_ajax_init')){
		function Listingpro_single_ajax_init(){
			
			
			wp_register_script('ajax-single-ajax', get_template_directory_uri() . '/assets/js/single-ajax.js', array('jquery') ); 
			 
			wp_enqueue_script('ajax-single-ajax');
			

			wp_localize_script( 'ajax-single-ajax', 'single_ajax_object', array( 
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
			));
			
		}
		if(!is_admin()){
			if(!is_singular('listing')){
				add_action('init', 'Listingpro_single_ajax_init');
			}
		}
	}
	
	
	
	
	/* ============== ListingPro Claim Ajax Process ============ */
	
	add_action('wp_ajax_listingpro_claim_list', 'listingpro_claim_list');
	add_action('wp_ajax_nopriv_listingpro_claim_list', 'listingpro_claim_list');
	if(!function_exists('listingpro_claim_list')){
		function listingpro_claim_list(){
			
			global $listingpro_options;
			$post_title = '';
			$post_url = '';
			$email1 = '';
			$message = '';
			$author_nicename = '';
			$author_url = '';
			$emailexist = false;
			
			$current_user = wp_get_current_user();
			$userID = $current_user->ID;
			$userData = get_user_by( 'id', $userID );
			$email1 = $userData->user_email;
		
			if( isset( $_POST[ 'formData' ] ) ) {
				parse_str( $_POST[ 'formData' ], $formData );
				
				$posttitle = '';
				$name1 = sanitize_text_field($formData['fullname']);
				$post_title = sanitize_text_field($formData['post_title']);
				$post_URL = sanitize_text_field($formData['post_url']);
				$posttitle = esc_html__('Claim for', 'listingpro').' ';
				$posttitle .= $post_title;
				
				$author_email = sanitize_text_field($formData['author_email']);
				$message = sanitize_text_field($formData['message']);
				$claimerphone = sanitize_text_field($formData['phone']);
				
				$post_id = sanitize_text_field($formData['post_id']);
				$post_author = get_post_field( 'post_author', $post_id );
				$user = get_user_by( 'id', $post_author );
				$usersname = $user->user_login;
				$authorEmail = $user->user_email;
				
				$claimerdetails = '';
				$claimerdetails .= $name1.' : ';
				$claimerdetails .= $email1.' : ';
				$claimerdetails .= $message;
				
				$status = 'pending';
				
				$claim_post = array(
				  'post_title'    => wp_strip_all_tags( $posttitle ),
				  'post_type'   => 'lp-claims',
				  'post_status'   => 'publish',
				);
				 
				$id = wp_insert_post( $claim_post );
				$current_user = get_post_field( 'post_author', $id );
				$current_usermeta = wp_get_current_user();
				$claimer_email = $current_usermeta->user_email;
				
				listing_set_metabox('details', $claimerdetails, $id);
				listing_set_metabox('claimer', $current_user, $id);
				listing_set_metabox('owner', $usersname, $id);
				listing_set_metabox('claim_status', $status, $id);
				listing_set_metabox('claimed_listing', $post_id, $id);
				listing_set_metabox('claimer_phone', $claimerphone, $id);
				
				$admin_email = '';
				$admin_email = get_option( 'admin_email' );
				$website_url = site_url();
				$website_name = get_option('blogname');
				$listing_title = $post_title;
				$listing_url = $post_URL;
				$headers[] = 'Content-Type: text/html; charset=UTF-8';
				/* ====for claimer=== */
				$c_mail_subject = $listingpro_options['listingpro_subject_listing_claimer'];
				$c_mail_body = $listingpro_options['listingpro_content_listing_claimer'];
				
				$c_mail_subject = str_replace('%website_url','%1$s', $c_mail_subject);
				$c_mail_subject = str_replace('%listing_title','%2$s', $c_mail_subject);
				$c_mail_subject = str_replace('%listing_url','%3$s', $c_mail_subject);
				$c_mail_subject = str_replace('%website_name','%4$s', $c_mail_subject);
				$c_mail_subject_a = sprintf( $c_mail_subject,$website_url,$listing_title,$listing_url, $website_name  );
				
				$c_mail_body = str_replace('%website_url','%1$s', $c_mail_body);
				$c_mail_body = str_replace('%listing_title','%2$s', $c_mail_body);
				$c_mail_body = str_replace('%listing_url','%3$s', $c_mail_body);
				$c_mail_body = str_replace('%website_name','%4$s', $c_mail_body);
				$c_mail_body_a = sprintf( $c_mail_body,$website_url,$listing_title,$listing_url, $website_name  );
				wp_mail( $claimer_email, $c_mail_subject_a, $c_mail_body_a, $headers);
				
				/* ====for Admin=== */
				$a_mail_subject = $listingpro_options['listingpro_subject_listing_claim_admin'];
				$a_mail_body = $listingpro_options['listingpro_content_listing_claim_admin'];
				
				$a_mail_subject = str_replace('%website_url','%1$s', $a_mail_subject);
				$a_mail_subject = str_replace('%listing_title','%2$s', $a_mail_subject);
				$a_mail_subject = str_replace('%listing_url','%3$s', $a_mail_subject);
				$a_mail_subject = str_replace('%website_name','%4$s', $a_mail_subject);
				$a_mail_subject_a = sprintf( $a_mail_subject,$website_url,$listing_title,$listing_url, $website_name  );
				
				$a_mail_body = str_replace('%website_url','%1$s', $a_mail_body);
				$a_mail_body = str_replace('%listing_title','%2$s', $a_mail_body);
				$a_mail_body = str_replace('%listing_url','%3$s', $a_mail_body);
				$a_mail_body = str_replace('%website_name','%4$s', $a_mail_body);
				$a_mail_body_a = sprintf( $a_mail_body,$website_url,$listing_title,$listing_url, $website_name  );
				wp_mail( $admin_email, $a_mail_subject_a, $a_mail_body_a, $headers);
				/* ====for Author=== */
				$athr_mail_subject = $listingpro_options['listingpro_subject_listing_author'];
				$athr_mail_body = $listingpro_options['listingpro_content_listing_author'];
				
				$athr_mail_subject = str_replace('%website_url','%1$s', $athr_mail_subject);
				$athr_mail_subject = str_replace('%listing_title','%2$s', $athr_mail_subject);
				$athr_mail_subject = str_replace('%listing_url','%3$s', $athr_mail_subject);
				$athr_mail_subject = str_replace('%website_name','%4$s', $athr_mail_subject);
				$athr_mail_subject_a = sprintf( $athr_mail_subject,$website_url,$listing_title,$listing_url, $website_name  );
				
				$athr_mail_body = str_replace('%website_url','%1$s', $athr_mail_body);
				$athr_mail_body = str_replace('%listing_title','%2$s', $athr_mail_body);
				$athr_mail_body = str_replace('%listing_url','%3$s', $athr_mail_body);
				$athr_mail_body = str_replace('%website_name','%4$s', $athr_mail_body);
				$athr_mail_body_a = sprintf( $athr_mail_body,$website_url,$listing_title,$listing_url, $website_name  );
				wp_mail( $authorEmail, $athr_mail_subject_a, $athr_mail_body_a, $headers);
				/* ====end for Author mail=== */
				
				$result = $id;
				
				echo json_encode(array('state'=>'<span class="alert alert-success">'.esc_html__('Claim has been submitted.','listingpro').'</span>','result'=>$result));
				exit();
			}
			

		}
	}

	/* ============== ListingPro Contact author Process ============ */
	
	
	add_action('wp_ajax_listingpro_contactowner', 'listingpro_contactowner');
	add_action('wp_ajax_nopriv_listingpro_contactowner', 'listingpro_contactowner');
	if(!function_exists('listingpro_contactowner')){
		function listingpro_contactowner(){
			global $listingpro_options;
			$post_id = '';
			$post_title = '';
			$post_url = '';
			$name = '';
			$email = '';
			$phone = '';
			$message = '';
			$authoremail = '';
			$authorID = '';
			$result = '';
			$error = array();
		
			if( isset( $_POST[ 'formData' ] ) ) {
				parse_str( $_POST[ 'formData' ], $formData );
				
				
				$post_id = sanitize_text_field($formData['post_id']);
				$post_title = get_the_title($post_id);
				$post_url = get_the_permalink($post_id);
				$name1 = sanitize_text_field($formData['name7']);
				$email = sanitize_text_field($formData['email7']);
				$phone = sanitize_text_field($formData['phone']);
				$message = sanitize_text_field($formData['message7']);
				$authorID = sanitize_text_field($formData['author_id']);
				$enableCaptcha = false;
				$processLead = true;
				$gCaptcha;
				
				if(isset($formData['g-recaptcha-response'])){
					if(!empty($formData['g-recaptcha-response'])){
						$enableCaptcha = true;
					}
					else{
						$processLead = false;
					}
				}
				else{
					$enableCaptcha = false;
					$processLead = true;
				}
				
				
				$keyResponse = '';
				
				if($enableCaptcha == true){
					if ( class_exists( 'cridio_Recaptcha' ) ){ 
										$keyResponse = cridio_Recaptcha_Logic::is_recaptcha_valid($formData['g-recaptcha-response']);
										if($keyResponse == false){
											$processLead = false;
										}
										else{
											$processLead = true;
										}
					}
				}
				
				if($processLead == true){
					$authoremail = sanitize_text_field($formData['author_email']);
					$toauthor = $authoremail;
					$subject =  $listingpro_options['listingpro_subject_lead_form'];
					$body =  $listingpro_options['listingpro_content_lead_form'];
					$body = str_replace('%listing_title','%1$s', $body);
					$body = str_replace('%sender_name','%2$s', $body);
					$body = str_replace('%sender_email','%3$s', $body);
					$body = str_replace('%sender_phone','%4$s', $body);
					$body = str_replace('%sender_message','%5$s', $body);

					$formated_mail_content = sprintf( $body,$post_title,$name1,$email,$phone,$message );
					$headers = "From: " . strip_tags($email) . "\r\n";
					$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$headers .= "Content-Type: text/html; charset=UTF-8";
					
					if(empty($email) || empty($message) || empty($name1)){
						$result = 'fail';
						if(empty($email)){
							$error['email'] = $email;
						}
						if(empty($message)){
							$error['message'] = $message;
						}
						if(empty($name1)){
							$error['name7'] = $name1;
						}
						echo json_encode(array('state'=>'<span class="alert alert-danger">'.esc_html__('Oh sorry! something missing','listingpro').'</span>','result'=>$result, 'errors'=>$error));
					}
					else{
						$result = wp_mail( $toauthor, $subject, $formated_mail_content,$headers); 
						if($result==true){
							$leadsCount = '';
							$leadsCount = get_user_meta( $authorID, 'leads_count', true );
							if( isset($leadsCount) ){
								$leadsCount = (int)$leadsCount + 1;
								update_user_meta($authorID, 'leads_count', $leadsCount);
							}
							else{
								$leadsCount = 0;
								update_user_meta($authorID, 'leads_count', $leadsCount);
							}
							
							
							
							
							/* saving activity in wp_options */
							
							$listing_id = $post_id;
							$listingData = get_post($listing_id);
							$authID = $listingData->post_author;
							$currentdate = date("Y-m-d h:i:a");
							$activityData = array();
							$activityData = array( array(
								'type'	=>	'lead',
								'actor'	=>	$name1,
								'reviewer'	=>	'',
								'listing'	=>	$listing_id,
								'rating'	=>	'',
								'time'	=>	$currentdate
							));
							
							$updatedActivitiesData = array();
							
							$lp_recent_activities = get_option( 'lp_recent_activities' );
							if( $lp_recent_activities!=false ){
								
								$existingActivitiesData = get_option( 'lp_recent_activities' );
								if (array_key_exists($authID, $existingActivitiesData)) {
									$currenctActivityData = $existingActivitiesData[$authID];
									if(count($currenctActivityData)>=20){
										$currenctActivityData = array_slice($currenctActivityData,1,20);
										$updatedActivityData = array_merge($currenctActivityData,$activityData);
									}
									else{
										$updatedActivityData = array_merge($currenctActivityData,$activityData);
									}
									$existingActivitiesData[$authID] = $updatedActivityData;
								}
								else{
									$existingActivitiesData[$authID] = $activityData;
								}
								$updatedActivitiesData = $existingActivitiesData;
							}
							else{
								$updatedActivitiesData[$authID] = $activityData;
							}
							update_option( 'lp_recent_activities', $updatedActivitiesData );
							
							
							
							
							
							
							echo json_encode(array('state'=>'<span class="alert alert-success">'.esc_html__('Email has been sent.','listingpro').'</span>','result'=>$result, 'errors'=>$error));
							
						}
						else{
							echo json_encode(array('state'=>'<span class="alert alert-danger">'.esc_html__('Something went wrong.','listingpro').'</span>','result'=>$result, 'errors'=>$error));
							
							

						}
						
						
					}
				}
				else{
					$result = 'fail';
					$error['email'] = $email;
					$error['message'] = $message;
					$error['name7'] = $name1;
					echo json_encode(array('state'=>'<span class="alert alert-danger">'.esc_html__('Please provide captcha information','listingpro').'</span>','result'=>$result, 'errors'=>$error));
				}
			}
			exit();
		}
	}
	
	/* =========================================change price plan============== */
	add_action('wp_ajax_listingpro_change_plan', 'listingpro_change_plan');
	add_action('wp_ajax_nopriv_listingpro_change_plan', 'listingpro_change_plan');
	if(!function_exists('listingpro_change_plan')){
		function listingpro_change_plan(){
			
			global $listingpro_options;
			$plan_id = $_POST['plan_id'];
			$listing_id = $_POST['listing_id'];
			$listing_status = $_POST['listing_status'];
			$checkout = $listingpro_options['payment-checkout'];
			$checkout_url = get_permalink( $checkout );
			$status = '';
			$ex_plan_price = '';
			$new_plan_price = '';
			$action = '';
			$subsc_status = '';
			$alertmsg = '';
			$ex_plan = listing_get_metabox_by_ID('Plan_id', $listing_id);
			if(!empty($ex_plan)){
				$ex_plan_price = get_post_meta($ex_plan, 'plan_price', true);
			}
			if(!empty($plan_id)){
				$new_plan_price = get_post_meta($plan_id, 'plan_price', true);
			}
			
			if($listing_status=="publish"){
				if($new_plan_price <= $ex_plan_price){
				$action = '		
								<form method="post"  action="'.$checkout_url.'">
								<input type="hidden" name="planid" value="'.$plan_id.'">
								<input type="hidden" name="listingid" value="'.$listing_id.'">
								<a href="" class="lp_change_plan_action" data-planid="'.$plan_id.'" data-listingid="'.$listing_id.'">'.esc_html__('Proceed', 'listingpro').'</a>
								<form>
								'
							;
				}
				else{
					$action = '
							<form method="post"  action="'.$checkout_url.'">
							<input type="hidden" name="planid" value="'.$plan_id.'">
							<input type="hidden" name="listingid" value="'.$listing_id.'">
							<input type="submit" value="'.esc_html__('Pay & Proceed', 'listingpro').'"/>
							</form>
							';
				}
				/* check if subscribed already */
				$alreadySubscribed = false;
				$uid = get_current_user_id();
				$userSubscriptions = get_user_meta($uid, 'listingpro_user_sbscr', true);
				if(!empty($userSubscriptions)){
					foreach($userSubscriptions as $key=>$subscription){
						$subscr_plan_id = $subscription['plan_id'];
						$subscr_listing_id = $subscription['listing_id'];
						if( ($subscr_plan_id == $ex_plan) &&($subscr_listing_id == $listing_id) ){
							$alreadySubscribed = true;
							break;
						}
					}
				}
				
				if($alreadySubscribed==true){
					$subsc_status = 'yes';
					$alertmsg = esc_html__('Alert! your existing plan attached with this listing has an active subscription, changing plan will cancel your subscription, make sure to change before proceed', 'listingpro');
				}
				/* end check if subscribed already */
				
			}
			else{
				
				$payment_status = lp_get_payment_status_column($listing_id);
				if( ($payment_status=="Success") && ($new_plan_price <= $ex_plan_price) ){
					$action = '
								<form method="post" action="'.$checkout_url.'">
								<input type="hidden" name="planid" value="'.$plan_id.'">
								<input type="hidden" name="listingid" value="'.$listing_id.'">
								<a href="" class="lp_change_plan_action" data-planid="'.$plan_id.'" data-listingid="'.$listing_id.'">'.esc_html__('Proceed', 'listingpro').'</a>
								</form>
								'
							;
				}
				
				elseif(!empty($new_plan_price)){
					$action = '
							<form method="post"  action="'.$checkout_url.'">
							<input type="hidden" name="planid" value="'.$plan_id.'">
							<input type="hidden" name="listingid" value="'.$listing_id.'">
							<input type="submit" value="'.esc_html__('Pay & Proceed', 'listingpro').'"/>
							</form>
							
						';
					listing_set_metabox('changed_planid', $plan_id, $listing_id);
				}
				else{
					$action = '
								<form method="post" action="'.$checkout_url.'">
								<input type="hidden" name="planid" value="'.$plan_id.'">
								<input type="hidden" name="listingid" value="'.$listing_id.'">
								<a href="" class="lp_change_plan_action" data-planid="'.$plan_id.'" data-listingid="'.$listing_id.'">'.esc_html__('Proceed', 'listingpro').'</a>
								</form>
								'
							;
				}
				
			}
			
			$data = array('planid'=>$plan_id, 'listing'=>$listing_id, 'status'=>$status, 'error'=>'', 'subscribed'=>$subsc_status, 'action'=>$action, 'actionurl'=>$checkout_url, 'listing_status'=>$listing_status, 'alertmsg'=>$alertmsg);
			die(json_encode($data));
			
		}
	}
	
	/* =========================================change plan proceeding============== */
	add_action('wp_ajax_listingpro_change_plan_proceeding', 'listingpro_change_plan_proceeding');
	add_action('wp_ajax_nopriv_listingpro_change_plan_proceeding', 'listingpro_change_plan_proceeding');
	if(!function_exists('listingpro_change_plan_proceeding')){
		function listingpro_change_plan_proceeding(){
			global $listingpro_options;
			$res = array();
			$plan_id = $_POST['plan_id'];
			$listing_id = $_POST['listing_id'];
			if(!empty($plan_id) && !empty($listing_id)){
				/* check if subscribed already */
				$changed_planid = listing_get_metabox_by_ID('changed_planid', $listing_id);
				if(!empty($changed_planid)){
					$ex_plan = listing_get_metabox_by_ID('Plan_id', $listing_id);
					lp_cancel_stripe_subscription($listing_id, $ex_plan);
					listing_set_metabox('Plan_id',$changed_planid, $listing_id);
					$planID = $changed_planid;	
					listing_set_metabox('changed_planid','', $listing_id);
				}
				/* end check if subscribed already */
				listing_set_metabox('Plan_id', $plan_id, $listing_id);
				$msg = '<span class="alert alert-success">'.esc_html__('Plan has been changed', 'listingpro').'</span>';
				$res = array('status'=>'success', 'message'=>$msg);
			}
			
			die(json_encode($res));
		}
	}
	/* =========================================cancel subscription proceeding============== */
	add_action('wp_ajax_listingpro_cancel_subscription_proceeding', 'listingpro_cancel_subscription_proceeding');
	add_action('wp_ajax_nopriv_listingpro_cancel_subscription_proceeding', 'listingpro_cancel_subscription_proceeding');
	if(!function_exists('listingpro_cancel_subscription_proceeding')){
		function listingpro_cancel_subscription_proceeding(){
			$response = array();
			global $listingpro_options;
			require_once THEME_PATH . '/include/stripe/stripe-php/init.php';
			$secritKey = '';
			$secritKey = $listingpro_options['stripe_secrit_key'];
			\Stripe\Stripe::setApiKey("$secritKey");
			if(isset($_POST['subscript_id'])){
				$subscrip_id = $_POST['subscript_id'];
				$subscription = \Stripe\Subscription::retrieve($subscrip_id);
				$subscription->cancel();
				
				$uid = get_current_user_id();
				$userSubscriptions = get_user_meta($uid, 'listingpro_user_sbscr', true);
				if(!empty($userSubscriptions)){
					foreach($userSubscriptions as $key=>$subscription){
						$subscr_id = $subscription['subscr_id'];
						if($subscr_id == $subscrip_id){
							unset($userSubscriptions[$key]);
							$headers[] = 'Content-Type: text/html; charset=UTF-8';
							/* user email */
							$author_obj = get_user_by('id', $uid);
							$user_email = $author_obj->user_email;
							$usubject = $listingpro_options['listingpro_subject_cancel_subscription'];
							$ucontent = $listingpro_options['listingpro_content_cancel_subscription'];
							wp_mail( $user_email, $usubject, $ucontent, $headers );
							/* admin email */
							$adminemail = get_option('admin_email');
							$asubject = $listingpro_options['listingpro_subject_cancel_subscription_admin'];
							$acontent = $listingpro_options['listingpro_content_cancel_subscription_admin'];
							wp_mail( $adminemail, $asubject, $acontent, $headers );
						}
					}
				}
				/* removing user meta */
				if(!empty($userSubscriptions)){
					update_user_meta($uid, 'listingpro_user_sbscr', $userSubscriptions);
				}
				else{
					delete_user_meta($uid, 'listingpro_user_sbscr');
				}
				/* end removing user meta */
				$response = array('status'=>'success', 'msg'=> esc_html__('you have unsubscribed successfully', 'listingpro'));
			}
			else{
				$response = array('status'=>'fail', 'msg'=> esc_html__('something went wrong', 'listingpro'));
			}
			die(json_encode($response));
		}
	}